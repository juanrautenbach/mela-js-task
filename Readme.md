### Instructions

Just open index.html and you are good to go.

### Online demo
Hosted on my external demo server

http://dev.julura.co.za:81

### External libraries used

JQuery 3.6.1 min
Fontawesome 5
Tailwind CSS 3.1.8

### Author
- Juan Rautenbach
- juan@julura.co.za
- +27 (0)81 787 8984
