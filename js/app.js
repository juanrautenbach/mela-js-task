

let search = $("#search");
let html, filter;
let clearBtn = $("#clearBtn");
let countries = getCountries();

search.val("");

// Switching on the lights
start();


function start(){
    buildCountryTable();
    $("#rowCount").text(countries.length);
}

function buildCountryTable() {

    for (let i = 0; i < countries.length; i++) {
        buildTableRows(countries[i]);
    }

    $("table tbody").html(html);
}

function buildTableRows(value) {
    html += `<tr class="hover:bg-teal-100">`;
    html += `<td class="w-12">`;
    html += `<input type="checkbox" class="h-4 w-4 accent-gray-700"/>`
    html += `</td>`;
    html += `<td class="text-left px-4">`;
    html += `${value.name}`;
    html += `</td>`;
    html += `<td>`;
    html += `${value.code}`;
    html += `</td>`;
    html += `</tr>`;

    return html;
}

$("input[type=checkbox]").on("click", function (){
    if (this.checked){
        $(this).closest('tr').addClass("bg-gray-200");
    }else{
        $(this).closest('tr').removeClass("bg-gray-200");
    }
});

$("#showSelectedBtn").on("click", function (){
   let checkedRows = $("input[type=checkbox]:checked").closest('tr');

   let returnedValues = '';

    for (let i = 0; i < checkedRows.length; i++) {
        returnedValues += checkedRows[i].lastChild.innerHTML + ',';
    }
    console.log(returnedValues);
    if (returnedValues.length > 0){
        alert(returnedValues.slice(0,-1));
    }else{
        alert("Oops.. You might have forgotten to select something. Give it another go.")
    }

});

search.on("keyup", function (){
    filter = $(this).val().toLowerCase();
    filterRows(filter);
});

function filterRows(filter){
    let td, searchVal;

    if (filter.length > 0){
        clearBtn.removeClass("hidden");
    }else{
        clearBtn.addClass("hidden");
    }

    let tr = $("tbody tr");

    for (let i = 0; i < tr.length; i++) {
        td = tr[i].children[1];
        if (td) {
            searchVal = td.innerText;
            if (searchVal.toLowerCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
    let recordCount = ($('tbody tr:not([style*="display: none"])').length);
    $("#rowCount").text(recordCount);
    if (recordCount === 0){
        console.log("No records to show");
    }
}

clearBtn.on("click", function (){
   search.val('');
   filter = '';
   filterRows(filter);
});





